# cloud-deepspeech
This is my coursework project for cloud-computing subject in university. 

## Desired architecture
WIP
![alt text](https://gitlab.com/SBloatedCat2/cloud-deepspeech/-/raw/main/architecture.png?inline=false)


## Shell-script
Shell script is tested with Oracle VirtualBox + Ubuntu server 20.04
## Docker-image
WIP
## Docker-compose
WIP
## Terraform
WIP
## Kubernetes
WIP
